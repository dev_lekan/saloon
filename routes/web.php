<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('/salon', 'PageController@salon');
Route::get('/salon_list','PageController@salon_list');

Route::get('/biz_index', 'bizController@index');

Route::get('/biz_admin', 'adminController@biz_admin');