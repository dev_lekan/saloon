<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
    	return view('index');
    }
    public function salon(){
    	return view('salon');
    }
    public function salon_list(){
    	return view('salon_list');
    }
}
