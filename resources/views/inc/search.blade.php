<div class="search-wrapper">
    <div class="banner-form">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="flight-tab">
                <form name="" id="search-form" class="d-flex-inline flex-nowrap" method="post" action="" onSubmit="">
                    <div class="row no-gutters search-form-div">
                        <div class="search-form-section">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="search-form-search"><i class="fas fa-search fa-fw"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Usluga / Salon" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Usluga / Salon'">
                            </div>
				        </div>
                        <div class="search-form-section d-none d-sm-flex">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="search-form-marker"><i class="fas fa-map-marker-alt fa-fw"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Lokacija" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Lokacija'">
                            </div>
				        </div>
                        <div class="search-form-section-btn">
                            <button type="submit" id="search-form-submit" class="fs-btn primary btn-lg"><i id="search-form-plane" class="fas fa-paper-plane fa-fw"></i></button>
                        </div>
                    </div>                      
                </form>
            </div>
        </div>
    </div>
</div>












