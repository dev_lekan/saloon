<header id="header" class="header">
    <div class="container">
        <div class="row align-items-center justify-content-between d-flex px-3">
            <div id="logo">
                <a href="/">
                    <!-- <img src="img/logo.png" alt="" title="" /> -->
                    <h3 id="logo-title" class="d-inline">Frizer</h3>
                </a>
            </div>
            <div id="login-container">
                <a href="#" data-toggle="modal" data-target="#login-form"><span class="far fa-user-circle"></span>Prijava / Registracija</a>
                <a href="/biz_index" class="btn fs-btn white-border">Za Salone</a>
            </div><!-- #nav-menu-container -->
            <button id="navbar-toggler" class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#nav-mob" aria-controls="nav-mob" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fas fa-bars"></span>
            </button>

        </div>
    </div>
    <div class="collapse navbar-collapse" id="nav-mob">
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="/"><span class="fas fa-home"></span>Početna</a>
            <a class="nav-item nav-link" href="/salon"><span class="far fa-calendar-check"></span>Kako naručiti termin</a>
            <a class="nav-item nav-link" href="#"><span class="far fa-question-circle"></span>Česta pitanja</a>
            <a class="nav-item nav-link" href="#"><span class="fas fa-briefcase"></span>Za salone</a>
            <a class="nav-item nav-link" href="#" data-toggle="modal" data-target="#login-form"><span class="far fa-user-circle"></span>Prijava / Registracija</a>
        </div>
    </div>
</header>