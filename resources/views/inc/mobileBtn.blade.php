<!-- TODO: display buttons acording to type of user -->


<!-- mobile buttons on bottom, visible only on small devices -->

<!-- user buttons -->
<div id="userBtns" class="btn-group btn-group-justified bottom-btns">
    <a href="#" class="btn bottom-btn bottom-book"><i class="far fa-heart"></i>
        <p>Omiljeni</p>
    </a>
    <a href="#" class="btn bottom-btn bottom-call"><i class="fas fa-search"></i>
        <p>Pretraži</p>
    </a>
    <a href="#" class="btn bottom-btn bottom-mail"><i class="far fa-calendar-check"></i>
        <p>Rezervacije</p>
    </a>
    <a href="#" class="btn bottom-btn bottom-whatsapp"><i class="far fa-user"></i>
        <p>Profil</p>
    </a>
</div>

<!-- niz-user-btns -->
<div id="bizUserBtns" class="btn-group btn-group-justified bottom-btns">
    <a href="#" class="btn bottom-btn bottom-book"><i class="fas fa-plus"></i>
        <p>Novi termin</p>
    </a>
    <a href="#" class="btn bottom-btn bottom-call"><i class="fas fa-exclamation-circle"></i>
        <p>Zahtjevi</p>
    </a>
    <a href="#" class="btn bottom-btn bottom-mail"><i class="far fa-calendar-check"></i>
        <p>Termini</p>
    </a>
    <a href="#" class="btn bottom-btn bottom-whatsapp"><i class="fas fa-tachometer-alt"></i>
        <p>Nadzorna</p>
    </a>
</div>