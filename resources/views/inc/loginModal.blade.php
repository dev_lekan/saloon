<div class="modal animated fadeIn" id="login-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold mt-3">Prijavite se</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <form id="login-form-fb" action="" method="post">
                    <div class="input-group mb-3">
                        <button id="submit" name="submit" type="submit" type="submit" class="fs-btn fb text-uppercase btn-lg btn-block">Prijavi se preko facebooka</button>
                    </div>
                </form>
                <form id="login-form" action="" method="post">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-user fa-1x"></i></span>
                        </div>
                        <input id="name" name="name" type="text" class="form-control" placeholder="E-adresa ili korisničko ime" aria-label="name" aria-describedby="basic-addon1" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-key fa-1x"></i></span>
                        </div>
                        <input id="password" name="password" type="password" class="form-control" placeholder="Lozinka" aria-label="password" aria-describedby="basic-addon2" required>
                    </div>
                    <div class="input-group mb-3">
                        <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#password-form">Zaboravili ste lozinku?</a>
                    </div>
                    <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Prijavi se</button>
                </form>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <p>Nemate korisnički račun? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#register-form">Registrirajte se!</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal animated fadeIn" id="register-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold mt-3">Registrirajte se</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <form id="login-form-fb" action="" method="post">
                    <div class="input-group mb-3">
                        <button id="submit" name="submit" type="submit" type="submit" class="fs-btn fb text-uppercase btn-lg btn-block">Registriraj se preko facebooka</button>
                    </div>
                </form>
                <form id="login-form" action="" method="post">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope fa-1x"></i></span>
                        </div>
                        <input id="email" name="email" type="email" class="form-control" placeholder="E-adresa" aria-label="email" aria-describedby="basic-addon1" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-user fa-1x"></i></span>
                        </div>
                        <input id="name" name="name" type="text" class="form-control" placeholder="Korisničko ime" aria-label="name" aria-describedby="basic-addon1" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-key fa-1x"></i></span>
                        </div>
                        <input id="password" name="password" type="password" class="form-control" placeholder="Lozinka" aria-label="password" aria-describedby="basic-addon2" required>
                    </div>
                    <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Registriraj se</button>
                </form>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <p>Imate korisnički račun? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#login-form">Prijavite se!</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal animated fadeIn" id="password-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold mt-3">Resetirajte lozinku</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <form id="login-form" action="" method="post">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope fa-1x"></i></span>
                        </div>
                        <input id="name" name="name" type="email" class="form-control" placeholder="E-adresa" aria-label="name" aria-describedby="basic-addon1" required>
                    </div>
                    <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Resetiraj lozinku</button>
                </form>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#login-form">Povratak na prijavu</a>
            </div>
        </div>
    </div>
</div>