<header id="header-fixed" class="header">
    <div class="container">
        <div class="row align-items-center justify-content-between d-flex px-3">
            <div id="logo">
                <a href="/">
                    <!-- <img src="img/logo.png" alt="" title="" /> -->
                    <h3 class="d-inline">Frizer</h3>
                </a>
            </div>
            <!-- user logged in -->
            <div id="login-container">
                <a id="user-logged-link" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="far fa-user-circle"></span>username123</a>
                <!-- dropdown menu -->
                <div id="user-logged-menu" class="dropdown-menu user-menu" aria-labelledby="user-menu-btn">
                    <a class="dropdown-item" href="/"><span class="fas fa-home fa-fw"></span>Početna</a>
                    <a class="dropdown-item" href="#"><span class="far fa-calendar-check fa-fw"></span>Moje rezervacije</a>
                    <a class="dropdown-item" href="#"><span class="far fa-comments fa-fw"></span>Moji komentari</a>
                    <a class="dropdown-item" href="#"><span class="fas fa-map-pin fa-fw"></span>Moje adrese</a>
                    <a class="dropdown-item" href="#"><span class="far fa-heart fa-fw"></span>Omiljeni saloni</a>
                    <a class="dropdown-item" href="#"><span class="fas fa-tags fa-fw"></span>Promo bodovi</a>
                    <a class="dropdown-item" href="#"><span class="fas fa-cog fa-fw"></span>Postavke profila</a>
                    <a class="dropdown-item" href="#"><span class="fas fa-sign-out-alt fa-fw"></span>Odjavi se</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/salon"><span class="far fa-calendar-check fa-fw"></span>Kako se naručiti</a>
                    <a class="dropdown-item" href="#"><span class="far fa-question-circle fa-fw"></span>Česta pitanja</a>
                </div>
                <!-- cart -->
                <a href="/biz_index" class="btn fs-btn dark"><span class="fas fa-shopping-cart"></span>0,00 kn</a>
            </div>
            <!-- toggle button for user menu -->
            <button class="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#nav-mob" aria-controls="nav-mob" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

        </div>
    </div>
    <!-- user collapsed menu, visible only on small devices -->
    <div class="collapse navbar-collapse" id="nav-mob">
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="/"><span class="fas fa-home"></span>Početna</a>
            <a class="nav-item nav-link" href="#"><span class="far fa-calendar-check fa-fw"></span>Moje rezervacije</a>
            <a class="nav-item nav-link" href="#"><span class="far fa-comments fa-fw"></span>Moji komentari</a>
            <a class="nav-item nav-link" href="#"><span class="fas fa-map-pin fa-fw"></span>Moje adrese</a>
            <a class="nav-item nav-link" href="#"><span class="far fa-heart fa-fw"></span>Omiljeni saloni</a>
            <a class="nav-item nav-link" href="#"><span class="fas fa-tags fa-fw"></span>Promo bodovi</a>
            <a class="nav-item nav-link" href="#"><span class="fas fa-cog fa-fw"></span>Postavke profila</a>
            <a class="nav-item nav-link" href="#"><span class="fas fa-sign-out-alt fa-fw"></span>Odjavi se</a>
            <div class="dropdown-divider"></div>
            <a class="nav-item nav-link" href="/salon"><span class="far fa-calendar-check fa-fw"></span>Kako se naručiti</a>
            <a class="nav-item nav-link" href="#"><span class="far fa-question-circle fa-fw"></span>Česta pitanja</a>
        </div>
    </div>
</header>