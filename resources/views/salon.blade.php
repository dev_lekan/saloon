@extends('inc.asset')
@section('content')
	@include('inc.nav')
	<div class="main-top">

	    <section id="salon-banner" class="salon-banner-area">
	        <div class="overlay overlay-bg"></div>
	        <div class="container">
	            <div class="row align-items-center justify-content-between">
	                <div class="col-12 salon-banner-content">
	                    <div class="my-5 text-center">
	                        <h1>Frizerski salon Diamond</h1>
	                        <p class="text-white link-nav"><span class="fa fa-arrow-left mr-2"></span> <a href="index.php"> Povratak na listu salona</a></p>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>

	    <div id="main" class="container">
	        <div class="row">
	            <div id="main-content" class="col-lg-8 pb-5">
	                <section id="salon" class="main-content-area-salon">
	                    <div class="container content-container">
	                        <div id="salon-info" class="mb-4">
	                            <div class="col-12">
	                                    <p><span class="fa fa-star checked"></span>
	                                        <span class="fa fa-star checked"></span>
	                                        <span class="fa fa-star checked"></span>
	                                        <span class="fa fa-star checked"></span>
	                                        <span class="fa fa-star"></span>
	                                        36 ocjena
	                                        <span class="fa fa-comments ml-3"></span> 43 komentara
	                                        <span class="fa fa-heart ml-3"></span> 12 omiljenih</p>
	                                <p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem inventore enim, laudantium earum corporis totam aperiam obcaecati molestiae numquam, sunt iure quisquam neque fuga! Iure mollitia quod error neque.</p>
	                            </div>
	                        </div>
	                        <div id="salon-galerija" class="mb-4">
	                            <div class="col-12">
	                                <h4 class="section-title">Slike</h4>
	                                <div class="row mt-3 no-gutters">
	                                    <?php
	                                            $images1 = array();
	                                            $files = glob("img/images/*.*");
	                                            foreach ($files as $image) {
	                                            $images1[] = $image;
	                                            }
	                                              
	                                            $images2 = array();
	                                            $files2 = glob("img/images/thumb*.*");
	                                            foreach ($files2 as $image) {
	                                            $images2[] = $image;
	                                            }
	                                            
	                                            $result = array_diff($images1, $images2);
	                                            $picnum = sizeof($result);
	                                            $lastthumb = end($images2);
	                                            
	                                            for( $i = 0, $count = count( $images2 ) - 1; $i < $count; $i++ ){
	                                                $showing = $images2[$i];

	                                                echo '<div class="col-md-4 col-6">
	                                                            <a href="' . $showing . '" data-lightbox="'.basename(dirname($showing)).'" data-title="'.basename($showing).'" title="Preview">
	                                                                <div class="content-box-pic">
	                                                                <div class="img-wrapper">
	                                                                    <img src="' . $showing . '" class="img-responsive" alt="'.basename(dirname($showing)).'" />
	                                                                </div>
	                                                                </div>
	                                                            </a>
	                                                            
	                                                      </div>'; 
	                                            }
	                                            
	                                                echo '<div class="col-md-4 col-6">
	                                                            <a href="' . $lastthumb . '" data-lightbox="'.basename(dirname($lastthumb)).'" data-title="'.basename($lastthumb).'" title="Preview">
	                                                                <div class="content-box-pic">
	                                                                <div class="img-wrapper">
	                                                                    <div class="overlay"></div>
	                                                                    <img src="' . $lastthumb . '" class="img-responsive" alt="'.basename(dirname($lastthumb)).'" />
	                                                                    <h4 class="">Pogledaj još ('.$picnum.')</h4>
	                                                                </div>
	                                                                </div>
	                                                            </a>
	                                                            
	                                                      </div>';



	                                            foreach($result as $hidden){
					
	                                             echo '<div class="d-none">
	                                                        <a href="' . $hidden . '" data-lightbox="'.basename(dirname($hidden)).'" data-title="'.basename($hidden).'" title="Preview">
	                                                        <img src="' . $hidden . '" class="img-fluid" alt="" />
	                                                        </a>
	                                                    </div>';
	                                            }
	                                        ?>
	                                </div>
	                            </div>
	                        </div>
	                        <div id="salon-usluge" class="mb-4">
	                            <div class="col-12">
	                                <h4 class="section-title">Usluge</h4>
	                                <div class="row no-gutters mt-3">
	                                    <div class="col-12">
	                                        <table id="table-frizure" class="table table-sm">
	                                            <tbody>
	                                                <tr>
	                                                    <td class="text-left">
	                                                        <p>žensko šišanje<span class="badge badge-danger akcija">AKCIJA!</span></p>
	                                                        <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                    </td>
	                                                    <td class="text-right td-flex">
	                                                        <div class="td-2 mr-3">
	                                                            <p>50,00 kn</p>
	                                                            <small>30 min</small>
	                                                        </div>
	                                                        <div class="td-1"><a href="#" data-toggle="modal" data-target="#bookModal" class="btn fs-btn small dark">Book</a></div>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="text-left">
	                                                        <p>Maska za kosu</p>
	                                                        <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                    </td>
	                                                    <td class="text-right td-flex">
	                                                        <div class="td-2 mr-3">
	                                                            <p>20,00 kn</p>
	                                                            <small>1.20 h</small>
	                                                        </div>
	                                                        <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="text-left">
	                                                        <p>Pranje kose</p>
	                                                        <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                    </td>
	                                                    <td class="text-right td-flex">
	                                                        <div class="td-2 mr-3">
	                                                            <p>30,00 kn</p>
	                                                            <small>10 min</small>
	                                                        </div>
	                                                        <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="text-left">
	                                                        <p>Pranje + fen ravno</p>
	                                                        <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                    </td>
	                                                    <td class="text-right td-flex">
	                                                        <div class="td-2 mr-3">
	                                                            <p>45,00 kn</p>
	                                                            <small>30 min</small>
	                                                        </div>
	                                                        <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="text-left">
	                                                        <p>Pranje + fen lokne</p>
	                                                        <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                    </td>
	                                                    <td class="text-right td-flex">
	                                                        <div class="td-2 mr-3">
	                                                            <p>55,00 kn</p>
	                                                            <small>35 min</small>
	                                                        </div>
	                                                        <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                    </td>
	                                                </tr>
	                                            </tbody>
	                                        </table>
	                                        <div class="collapse" id="collapseTable">
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>žensko šišanje<span class="badge badge-danger akcija">AKCIJA!</span></p>
	                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>50,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Maska za kosu</p>
	                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>20,00 kn</p>
	                                                                <small>1.20 h</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                    <div class="col-12 text-center btn-more-services">
	                                        <a id="collapseTableBtn" class="btn fs-btn medium primary" data-toggle="collapse" href="#collapseTable" role="button" aria-expanded="false" aria-controls="collapseTable">Prikaži više</a>
	                                    </div>



	                                </div>
	                            </div>
	                        </div>
	                        <div id="salon-komentari" class="mb-4">
	                            <div class="col-12">
	                                <h4 class="section-title">Komentari</h4>
	                            </div>
	                            <div class="col-12 mt-3">
	                                <div class="container">
	                                    <div class="row align-items-center">
	                                        <div class="col-md-6 text-center">
	                                            <div class="ratings-left-ukupno">
	                                                <h3>Ukupno</h3>
	                                            </div>
	                                            <div class="ratings-left-rating">
	                                                <h1>4,8</h1>
	                                            </div>
	                                            <div class="ratings-left-num">
	                                                <p>(146 ocjena)</p>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <p class="ratings-stars-row">
	                                                <span class="zvijezdice-title">5 zvijezdica</span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="ratings-ocjena">(120)</span>
	                                            </p>
	                                            <p class="ratings-stars-row">
	                                                <span class="zvijezdice-title">4 zvijezdice</span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="ratings-ocjena">(20)</span>
	                                            </p>
	                                            <p class="ratings-stars-row">
	                                                <span class="zvijezdice-title">3 zvijezdice</span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="ratings-ocjena">(0)</span>
	                                            </p>
	                                            <p class="ratings-stars-row">
	                                                <span class="zvijezdice-title">2 zvijezdice</span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="ratings-ocjena">(1)</span>
	                                            </p>
	                                            <p class="ratings-stars-row">
	                                                <span class="zvijezdice-title">1 zvijezdica</span>
	                                                <span class="fa fa-star checked"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="fa fa-star"></span>
	                                                <span class="ratings-ocjena">(5)</span>
	                                            </p>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-12">
	                                <div class="row mt-3">
	                                    <div class="col-12 komentar-div">
	                                        <div class="row no-gutters">
	                                            <div class="col-12 komentar-header">
	                                                <div class="komentar-header-left">
	                                                    <h4 class="komentar-username">Frane iz Omisa</h4>
	                                                    <p><span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star"></span></p>
	                                                </div>
	                                                <div class="komentar-header-right">
	                                                    <small class="komentar-date">14.02.2020.</small>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 komentar-body">
	                                                <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore earum delectus molestias aut dolor fuga, saepe atque exercitationem hic. Modi quisquam sit, numquam repellendus asperiores incidunt quam illum nostrum sunt. ipsum dolor sit amet, consectetur adipisicing elit. Magni fugit impedit aliquid nostrum ratione reiciendis incidunt, distinctio labore neque nihil iste voluptate animi quis eum provident sapiente sint rerum non.</p>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="col-12 komentar-div">
	                                        <div class="row no-gutters">
	                                            <div class="col-12 komentar-header">
	                                                <div class="komentar-header-left">
	                                                    <h4 class="komentar-username">Frane iz Omisa</h4>
	                                                    <p><span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star"></span></p>
	                                                </div>
	                                                <div class="komentar-header-right">
	                                                    <small class="komentar-date">14.02.2020.</small>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 komentar-body">
	                                                <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="col-12 komentar-div">
	                                        <div class="row no-gutters">
	                                            <div class="col-12 komentar-header">
	                                                <div class="komentar-header-left">
	                                                    <h4 class="komentar-username">Frane iz Omisa</h4>
	                                                    <p><span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star"></span></p>
	                                                </div>
	                                                <div class="komentar-header-right">
	                                                    <small class="komentar-date">14.02.2020.</small>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 komentar-body">
	                                                <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure minus ducimus voluptatem cupiditate non consectetur maxime, amet similique soluta. Cumque animi obcaecati autem laudantium deleniti, odio sint sunt eius rem.</p>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="col-12 komentar-div">
	                                        <div class="row no-gutters">
	                                            <div class="col-12 komentar-header">
	                                                <div class="komentar-header-left">
	                                                    <h4 class="komentar-username">Frane iz Omisa</h4>
	                                                    <p><span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star"></span></p>
	                                                </div>
	                                                <div class="komentar-header-right">
	                                                    <small class="komentar-date">14.02.2020.</small>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 komentar-body">
	                                                <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="col-12 komentar-div">
	                                        <div class="row no-gutters">
	                                            <div class="col-12 komentar-header">
	                                                <div class="komentar-header-left">
	                                                    <h4 class="komentar-username">Frane iz Omisa</h4>
	                                                    <p><span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star checked"></span>
	                                                        <span class="fa fa-star"></span></p>
	                                                </div>
	                                                <div class="komentar-header-right">
	                                                    <small class="komentar-date">14.02.2020.</small>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 komentar-body">
	                                                <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure minus ducimus voluptatem cupiditate non consectetur maxime, amet similique soluta. Cumque animi obcaecati autem laudantium deleniti, odio sint sunt eius rem.</p>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-12">
	                                <nav aria-label="Page navigation">
	                                    <ul class="pagination justify-content-center">
	                                        <li class="page-item align-self-center">
	                                            <a class="page-link" href="#" aria-label="Previous">
	                                                <span aria-hidden="true">&laquo;</span>
	                                                <span class="sr-only">Previous</span>
	                                            </a>
	                                        </li>
	                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
	                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
	                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
	                                        <li class="page-item">
	                                            <a class="page-link" href="#" aria-label="Next">
	                                                <span aria-hidden="true">&raquo;</span>
	                                                <span class="sr-only">Next</span>
	                                            </a>
	                                        </li>
	                                    </ul>
	                                </nav>
	                            </div>
	                        </div>
	                    </div>
	                </section>
	            </div>
	            <div id="sidebar" class="col-lg-4 pb-5">
	                <div class="container stickey-top">
	                    <div class="row">
	                        <div id="sidebar-radno-vrijeme" class="col-12 content-container">
	                            <ul class="fa-ul">
	                                <li class="mb-2">
	                                    <span class="fa-li"><i class="fa fa-map-marker"></i></span>
	                                    <b>Adresa: </b>Slavićeva 1, Split
	                                </li>
	                                <li class="mb-2"><span class="fa-li"><i class="fa fa-phone"></i></span><b>Kontakt: </b>0987654321</li>
	                                <li class="mb-2">
	                                    <span class="fa-li"><i class="fa fa-clock"></i></span>
	                                    <b>Radno vrijeme: </b>
	                                    <table id="radno-vrijeme" class="table table-sm table-borderless">
	                                        <tr>
	                                            <td class="dan">Ponedjeljak</td>
	                                            <td class="vrijeme">08.00 - 20.00</td>
	                                        </tr>
	                                        <tr>
	                                            <td class="dan">Utorak</td>
	                                            <td class="vrijeme">08.00 - 20.00</td>
	                                        </tr>
	                                        <tr>
	                                            <td class="dan">Srijeda</td>
	                                            <td class="vrijeme">08.00 - 20.00</td>
	                                        </tr>
	                                        <tr>
	                                            <td class="dan">Četvrtak</td>
	                                            <td class="vrijeme">08.00 - 20.00</td>
	                                        </tr>
	                                        <tr>
	                                            <td class="dan">Petak</td>
	                                            <td class="vrijeme">08.00 - 20.00</td>
	                                        </tr>
	                                        <tr>
	                                            <td class="dan">Subota</td>
	                                            <td class="vrijeme">Zatvoreno</td>
	                                        </tr>
	                                        <tr>
	                                            <td class="dan">Nedjelja</td>
	                                            <td class="vrijeme">Zatvoreno</td>
	                                        </tr>
	                                    </table>
	                                </li>
	                            </ul>
	                        </div>
	                        <div id="sidebar-mapa" class="col-12">
	                            <div class="embed-responsive embed-responsive-4by3 my-4">
	                                <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2893.578713179132!2d16.437468680163445!3d43.51112337922857!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13355dfdec346fd7%3A0x3e9fbf8883e3f00b!2s%22Diamond%22%20Hair%20%26%20Beauty%20Studio!5e0!3m2!1sen!2shr!4v1572749639559!5m2!1sen!2shr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	                            </div>
	                        </div>
	                        <div id="sidebar-info" class="col-12 content-container">
	                            <ul class="fa-ul">
	                                <li class="mb-2"><span class="fa-li"><i class="fa fa-credit-card"></i></span><b>Način plačanja: </b>gotovina, kartice</li>
	                                <li class="mb-2"><span class="fa-li"><i class="fas fa-walking"></i></span><b>Dolazak doma: </b>nije dostupno</li>
	                                <li class="mb-2"><span class="fa-li"><i class="fas fa-wifi"></i></span><b>Wifi: </b>dostupno</li>
	                                <li class="mb-2"><span class="fa-li"><i class="fas fa-parking"></i></span><b>Parking: </b>dostupno</li>
	                                <li class="mb-2"><span class="fa-li"><i class="fab fa-accessible-icon"></i></span><b>Pristup za inv. osobe: </b>dostupno</li>
	                            </ul>
	                        </div>
	                        <div id="sidebar-social" class="col-12 content-container text-center">
	                            <ul class="social-network social-circle">
	                                <li><a href="#" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
	                                <li><a href="#" class="icoInstagram" title="Instagram"><i class="fab fa-instagram"></i></a></li>
	                                <li><a href="#" class="icoWebsite" title="Website"><i class="fas fa-globe"></i></a></li>
	                            </ul>
	                        </div>
	                    </div>
	                </div>
	            </div>
	                <!-- Popularni saloni /-->
	             <section id="popularni-saloni" class="main-content-area col-lg-12 pb-5">
	                <div class="container content-container">
	                    <div class="row h-100">
	                        <div class="col-12">
	                            <div class="mb-5">
	                                <h4 class="float-left section-title">Slični saloni</h4>
	                                <h5 class="float-right"><a href="#">Prikaži sve</a></h5>
	                            </div>




	                        </div>
	                    </div>
	                    <div class="row no-gutters owl-container h-100">
	                        <div class="active-recent-blog-carusel owl-theme">
	                            <div class="single-recent-blog-post item">
	                                <a href="salon.php">
	                                    <div class="content-box">
	                                        <div class="img-wrapper">
	                                            <img src="img/salon.jpg" alt="" class="img-responsive">
	                                            <h3 class="akcija"><span class="badge badge-danger">AKCIJA!</span></h3>
	                                        </div>
	                                        <div class="container">
	                                            <div class="row">
	                                                <div class="col-12">
	                                                    <div class="index-salon-text">
	                                                        <h5>Frizerski salon Diamond</h5>
	                                                        <p> Slavićeva 1, Split</p>
	                                                    </div>
	                                                </div>
	                                                <div class="col-12">
	                                                    <div class="index-salon-rates">
	                                                        <div class="index-salon-stars">
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                        </div>
	                                                        <div>
	                                                            <small>142 ocjene</small>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </a>
	                            </div>
	                            <div class="single-recent-blog-post item">
	                                <a href="salon.php">
	                                    <div class="content-box">
	                                        <div class="img-wrapper">
	                                            <img src="img/salon.jpg" alt="" class="img-responsive">
	                                            <h3 class="akcija"><span class="badge badge-danger">AKCIJA!</span></h3>
	                                        </div>
	                                        <div class="container">
	                                            <div class="row">
	                                                <div class="col-12">
	                                                    <div class="index-salon-text">
	                                                        <h5>Frizerski salon Diamond</h5>
	                                                        <p> Slavićeva 1, Split</p>
	                                                    </div>
	                                                </div>
	                                                <div class="col-12">
	                                                    <div class="index-salon-rates">
	                                                        <div class="index-salon-stars">
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                        </div>
	                                                        <div>
	                                                            <small>142 ocjene</small>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </a>
	                            </div>
	                            <div class="single-recent-blog-post item">
	                                <a href="salon.php">
	                                    <div class="content-box">
	                                        <div class="img-wrapper">
	                                            <img src="img/salon.jpg" alt="" class="img-responsive">
	                                            <h3 class="akcija"><span class="badge badge-danger">AKCIJA!</span></h3>
	                                        </div>
	                                        <div class="container">
	                                            <div class="row">
	                                                <div class="col-12">
	                                                    <div class="index-salon-text">
	                                                        <h5>Frizerski salon Diamond</h5>
	                                                        <p> Slavićeva 1, Split</p>
	                                                    </div>
	                                                </div>
	                                                <div class="col-12">
	                                                    <div class="index-salon-rates">
	                                                        <div class="index-salon-stars">
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                        </div>
	                                                        <div>
	                                                            <small>142 ocjene</small>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </a>
	                            </div>
	                            <div class="single-recent-blog-post item">
	                                <a href="salon.php">
	                                    <div class="content-box">
	                                        <div class="img-wrapper">
	                                            <img src="img/salon.jpg" alt="" class="img-responsive">
	                                            <h3 class="akcija"><span class="badge badge-danger">AKCIJA!</span></h3>
	                                        </div>
	                                        <div class="container">
	                                            <div class="row">
	                                                <div class="col-12">
	                                                    <div class="index-salon-text">
	                                                        <h5>Frizerski salon Diamond</h5>
	                                                        <p> Slavićeva 1, Split</p>
	                                                    </div>
	                                                </div>
	                                                <div class="col-12">
	                                                    <div class="index-salon-rates">
	                                                        <div class="index-salon-stars">
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                            <span class="fa fa-star checked"></span>
	                                                        </div>
	                                                        <div>
	                                                            <small>142 ocjene</small>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </a>
	                            </div>
	                        </div>
	                        <div class="owl-theme">
	                            <div class="owl-controls">
	                                <div class="custom-nav owl-nav text-center h-100 align-items-center"></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </section>
	        </div>
	    </div>

	</div>

	<div class="modal animated fadeIn" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
	            <div class="modal-header text-center">
	                <h4 class="modal-title w-100 font-weight-bold">Odaberi datum</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body mx-3">
	                    <div id="calendar-container" class="input-group mb-3">
	                        <div id="datepicker" data-date="12/03/2012"></div>
	                        <input type="hidden" id="my_hidden_input">
	                    </div>
	            </div>
	            <div id="book-modal-footer" class="modal-footer">
	                <div class="dodaj-uslugu mb-3">
	                    <table id="table-odabrane-usluge" class="table table-sm table-borderless mb-3">
	                        <tbody>
	                            <tr>
	                                <td class="text-left">
	                                    <p>žensko šišanje + feniranje + još neka usluga</p>
	                                </td>
	                                <td class="text-right td-flex">
	                                    <div class="td-2 mr-3">
	                                        <p>150,00 kn</p>
	                                    </div>
	                                </td>
	                            </tr>
	                        </tbody>
	                    </table>
	                    <a class="dodaj-uslugu" href="#" data-dismiss="modal" data-toggle="modal" data-target="#dodajUsluguModal"><span class="fa fa-plus mr-3"></span>dodaj uslugu</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal animated fadeIn" id="terminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
	            <div class="modal-header text-center">
	                <button type="button" class="prev" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true" class="fas fa-arrow-left"></span>
	                </button>
	                <h4 class="modal-title w-100 font-weight-bold">Odaberi termin</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	                <div class="termin-container">
	                    <p>Ujutro</p>
	                    <div class="termin-wrapper">
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">09.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">09.30</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">10.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">10.30</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">11.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">11.30</a>
	                        </div>
	                    </div>
	                </div>
	                <div class="termin-container">
	                    <p>Popodne</p>
	                    <div class="termin-wrapper">
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">12.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">12.30</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">13.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">13.30</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">14.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">14.30</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">15.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">15.30</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">16.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">16.30</a>
	                        </div>
	                    </div>
	                </div>
	                <div class="termin-container">
	                    <p>Navečer</p>
	                    <div class="termin-wrapper">
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">17.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">17.30</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">18.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">18.30</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">19.00</a>
	                        </div>
	                        <div class="termin">
	                            <a class="btn fs-btn dark-border small">19.30</a>
	                        </div>
	                    </div>
	                </div>
	                <div class="select-radnik-container">
	                    <select name="" id="">
	                        <option value="svi radnici">Svi saloni</option>
	                        <option value="username1">Mall of Split</option>
	                        <option value="username2">Slavićeva 1</option>
	                    </select>
	                    <select name="" id="">
	                        <option value="svi radnici">svi radnici</option>
	                        <option value="username1">Radnik 1</option>
	                        <option value="username2">Radnik 2</option>
	                    </select>
	                </div>
	            </div>
	            <div id="book-modal-footer" class="modal-footer">
	                <div class="dodaj-uslugu mb-3">
	                    <table id="table-odabrane-usluge" class="table table-sm table-borderless mb-3">
	                        <tbody>
	                            <tr>
	                                <td class="text-left">
	                                    <p>žensko šišanje + feniranje + još neka usluga</p>
	                                </td>
	                                <td class="text-right td-flex">
	                                    <div class="td-2 mr-3">
	                                        <p>150,00 kn</p>
	                                    </div>
	                                </td>
	                            </tr>
	                        </tbody>
	                    </table>
	                    <a class="dodaj-uslugu" href="#" data-dismiss="modal" data-toggle="modal" data-target="#dodajUsluguModal"><span class="fa fa-plus mr-3"></span>dodaj uslugu</a>
	                </div>
	                <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block" href="#" data-dismiss="modal" data-toggle="modal" data-target="#potvrdiUsluguModal">Potvrdi</button>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal animated fadeIn" id="potvrdiUsluguModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
	            <div class="modal-header text-center">
	                <button type="button" class="prev" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true" class="fas fa-arrow-left"></span>
	                </button>
	                <h4 class="modal-title w-100 font-weight-bold">Rezerviraj termin</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	                <table id="table-potvrdi-uslugu" class="table table-sm table-borderless mb-3">
	                    <tbody>
	                         <tr>
	                            <td class="text-left">
	                                <p class="potvrdi-uslugu-salon">Frizerski salon Diamond</p>
	                                <small class="potvrdi-uslugu-adresa">Slavićeva 1</small>
	                            </td>
	                            <td class="text-right">
	                                <div class="">
	                                    <a class="potvrdi-uslugu-link">uredi</a>
	                                </div>
	                            </td>
	                        </tr>
	                         <tr>
	                            <td class="text-left">
	                                <p class="potvrdi-uslugu-datum">Srijeda, 14. veljače</p>
	                                <p>14.00 - 14.30</p>
	                            </td>
	                            <td class="text-right">
	                                <div class="">
	                                    <a class="potvrdi-uslugu-link">uredi</a>
	                                </div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p class="potvrdi-uslugu-usluga">žensko šišanje + feniranje + još neka usluga</p>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2">
	                                    <p class="potvrdi-uslugu-cijena">120,00 kn</p>
	                                </div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	                <a class="dodaj-uslugu" href="#" data-dismiss="modal" data-toggle="modal" data-target="#dodajUsluguModal">
	                <span class="far fa-edit mr-3"></span>Dodaj bilješku salonu</a>
	            </div>
	            <div id="book-modal-footer" class="modal-footer">
	                <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block" href="#" data-dismiss="modal" data-toggle="modal" data-target="#potvrdiUsluguModal">Rezerviraj</button>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal animated fadeIn" id="dodajUsluguModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
	            <div class="modal-header text-center">
	                <h4 class="modal-title w-100 font-weight-bold mt-3">Dodaj uslugu</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	                <table id="table-sve-usluge" class="table table-sm table-borderless mb-3">
	                    <tbody>
	                        <tr>
	                            <td class="text-left">
	                                <p>žensko šišanje<span class="badge badge-danger akcija">AKCIJA!</span></p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>50,00 kn</p>
	                                    <small>30 min</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark" data-toggle="modal" data-target="#bookModal">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>Maska za kosu</p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>20,00 kn</p>
	                                    <small>1.20 h</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>Pranje kose</p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>30,00 kn</p>
	                                    <small>10 min</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>Pranje + fen ravno</p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>45,00 kn</p>
	                                    <small>30 min</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>Pranje + fen lokne</p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>55,00 kn</p>
	                                    <small>35 min</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>žensko šišanje<span class="badge badge-danger akcija">AKCIJA!</span></p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>50,00 kn</p>
	                                    <small>30 min</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark" data-toggle="modal" data-target="#bookModal">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>Maska za kosu</p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>20,00 kn</p>
	                                    <small>1.20 h</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>Pranje kose</p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>30,00 kn</p>
	                                    <small>10 min</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>Pranje + fen ravno</p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>45,00 kn</p>
	                                    <small>30 min</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark">Dodaj</a></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td class="text-left">
	                                <p>Pranje + fen lokne</p>
	                                <small>Kratki opis usluge da nije duži od ovog.</small>
	                            </td>
	                            <td class="text-right td-flex">
	                                <div class="td-2 mr-3">
	                                    <p>55,00 kn</p>
	                                    <small>35 min</small>
	                                </div>
	                                <div class="td-1"><a class="btn fs-btn small dark">Dodaj</a></div>
	                            </td>
	                        </tr>
	                    </tbody>

	                </table>
	            </div>
	            <div id="book-modal-footer" class="modal-footer">
	            </div>
	        </div>
	    </div>
	</div>
	@include('inc.footer')
@endsection