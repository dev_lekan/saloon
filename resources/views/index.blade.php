@extends('inc.asset')
@section('content')
	@include('inc.nav')
	<div class="main-top">

        <!-- Trazilica banner -->
        <section id="index-banner" class="index-banner-area">
            <div class="overlay overlay-bg"></div>
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-12 index-banner-content">
                        <div class="mt-5 text-center">
                            <h1>Pronađite salon</h1>
                            <h4>Pronađite svoj omiljeni salon i rezervirajte termin online.</h4>
                        </div>
                        @include('inc.search')
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg main-menu">
                <div class="container">
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav nav-fill w-100">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Frizerski saloni <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/saloni-list">Brijačnice</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/saloni-list">Nokti</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/saloni-list">Beauty saloni</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/saloni-list">Obrve & trepavice</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/saloni-list">Masaže</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/saloni-list">Makeup</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/saloni-list">Spa & Wellness</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </section>

        <!-- Popularni saloni /-->
        <section id="popularni-saloni" class="main-content-area py-5">
            <div class="container content-container">
                <div class="row h-100">
                    <div class="col-12">
                        <div class="mb-5">
                            <h4 class="float-left section-title">Istaknuti saloni</h4>
                            <a id="collapseTableBtn" class="btn fs-btn medium primary float-right" data-toggle="collapse" href="#collapseTable" role="button" aria-expanded="false" aria-controls="collapseTable">Prikaži više</a>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters owl-container h-100">
                    <div class="active-recent-blog-carusel owl-theme">
                        <div class="single-recent-blog-post item">
                            <a href="/salon">
                                <div class="content-box">
                                    <div class="img-wrapper">
                                        <img src="img/salon.jpg" alt="" class="img-responsive">
                                        <h3 class="akcija"><span class="badge badge-danger">AKCIJA!</span></h3>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="index-salon-text">
                                                    <h5>Frizerski salon Diamond</h5>
                                                    <p> Slavićeva 1, Split</p>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="index-salon-rates">
                                                    <div class="index-salon-stars">
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                    </div>
                                                    <div>
                                                        <small>142 ocjene</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-recent-blog-post item">
                            <a href="/salon">
                                <div class="content-box">
                                    <div class="img-wrapper">
                                        <img src="img/salon.jpg" alt="" class="img-responsive">
                                        <h3 class="akcija"><span class="badge badge-danger">AKCIJA!</span></h3>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="index-salon-text">
                                                    <h5>Frizerski salon Diamond</h5>
                                                    <p> Slavićeva 1, Split</p>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="index-salon-rates">
                                                    <div class="index-salon-stars">
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                    </div>
                                                    <div>
                                                        <small>142 ocjene</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-recent-blog-post item">
                            <a href="/salon">
                                <div class="content-box">
                                    <div class="img-wrapper">
                                        <img src="img/salon.jpg" alt="" class="img-responsive">
                                        <h3 class="akcija"><span class="badge badge-danger">AKCIJA!</span></h3>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="index-salon-text">
                                                    <h5>Frizerski salon Diamond</h5>
                                                    <p> Slavićeva 1, Split</p>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="index-salon-rates">
                                                    <div class="index-salon-stars">
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                    </div>
                                                    <div>
                                                        <small>142 ocjene</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-recent-blog-post item">
                            <a href="/salon">
                                <div class="content-box">
                                    <div class="img-wrapper">
                                        <img src="img/salon.jpg" alt="" class="img-responsive">
                                        <h3 class="akcija"><span class="badge badge-danger">AKCIJA!</span></h3>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="index-salon-text">
                                                    <h5>Frizerski salon Diamond</h5>
                                                    <p> Slavićeva 1, Split</p>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="index-salon-rates">
                                                    <div class="index-salon-stars">
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                    </div>
                                                    <div>
                                                        <small>142 ocjene</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="custom-nav owl-nav text-center h-100 align-items-center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- poziv na akciju /-->
        <section id="call-to-action">
            <div class="overlay-action">
                <div class="container py-5">
                    <div class="row align-items-center">
                        <div class="col-md-10 py-5">
                            <h1 class="title">Skini mobilnu aplikaciju</h1>
                            <p class="mb-4">Skini aplikaciju, skupljaj nagradne bodove i iskoristi ih u svom omiljenom salonu.</p>
                        </div>
                        <div class="col-2 h-100">
                            <a class="btn fs-btn dark">Skini aplikaciju</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('inc.footer')
@endsection