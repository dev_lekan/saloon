@extends('inc.asset')
@section('content')
	@include('inc.header_fixed')

	<div class="main-top">

	    <div id="main-content-area" class="container">
	        <div class="row no-gutters">
	            <div id="main-content" class="col-lg-9 col-xl-9 order-lg-12">
	                <div id="salon-container" class="container">
	                    <div class="row">
	                        <div class="col-12">
	                            <div class="tab-content">
	                                <!-- nadzorna ploca -->
	                                <div class="tab-pane fade show active" id="biz-nadzorna" role="tabpanel">
	                                    <div class="container">
	                                        <div class="row">
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Nadzorna ploča</h4>
	                                                    <div class="btn-group my-3 my-sm-0">
	                                                        <a class="btn fs-btn dark">Dan</a>
	                                                        <a class="btn fs-btn dark">Tjedan</a>
	                                                        <a class="btn fs-btn dark">Mjesec</a>
	                                                    </div>
	                                                </div>
	                                                <div class="container">
	                                                    <div class="row">
	                                                        <div class="col-12 col-lg-4 col-sm-6">
	                                                            <div class="overview-box">
	                                                                <div id="ob-1" class="container">
	                                                                    <div class="overview-title-box clearfix">
	                                                                        <div class="icon">
	                                                                            <i class="far fa-calendar-check"></i>
	                                                                        </div>
	                                                                        <div class="text">
	                                                                            <h4>370</h4>
	                                                                            <p>Rezervacije</p>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="overview-chart-box">
	                                                                        <canvas id="rezervacije-chart"></canvas>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 col-lg-4 col-sm-6">
	                                                            <div class="overview-box">
	                                                                <div id="ob-2" class="container">
	                                                                    <div class="overview-title-box clearfix">
	                                                                        <div class="icon">
	                                                                            <i class="far fa-clock"></i>
	                                                                        </div>
	                                                                        <div class="text">
	                                                                            <h4>487</h4>
	                                                                            <p>Sati rada</p>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="overview-chart-box">
	                                                                        <canvas id="sati-rada-chart"></canvas>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 col-lg-4 col-sm-6">
	                                                            <div class="overview-box">
	                                                                <div id="ob-3" class="container">
	                                                                    <div class="overview-title-box clearfix">
	                                                                        <div class="icon">
	                                                                            <i class="fas fa-wallet"></i>
	                                                                        </div>
	                                                                        <div class="text">
	                                                                            <h4>1850000 kn</h4>
	                                                                            <p>Prihodi</p>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="overview-chart-box">
	                                                                        <canvas id="prihodi-chart"></canvas>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Zahtjevi</h4>
	                                                    <a class="btn fs-btn dark">Vidi sve</a>
	                                                </div>
	                                                <div class="container">
	                                                    <div class="row">
	                                                        <div class="col-12 rezervacije-div">
	                                                            <div class="rezervacije-left">
	                                                                <p class="rezervacije-date">14.02.2020. u 12.00h</p>
	                                                                <p>Trajni lak + čupanje obrva + feniranje + pramenovi</p>
	                                                                <p class="rezervacije-username">Username123</p>
	                                                            </div>
	                                                            <div class="rezervacije-right">
	                                                                <div class="rezervacije-btns">
	                                                                    <a class="btn fs-btn small"><span class="fas fa-check"></span></a>
	                                                                    <a class="btn fs-btn small"><span class="fas fa-times"></span></a>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 rezervacije-div">
	                                                            <div class="rezervacije-left">
	                                                                <p class="rezervacije-date">14.02.2020. u 13.00h</p>
	                                                                <p>feniranje</p>
	                                                                <p class="rezervacije-username">Username123</p>
	                                                            </div>
	                                                            <div class="rezervacije-right">
	                                                                <div class="rezervacije-btns">
	                                                                    <a class="btn fs-btn small"><span class="fas fa-check"></span></a>
	                                                                    <a class="btn fs-btn small"><span class="fas fa-times"></span></a>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 rezervacije-div">
	                                                            <div class="rezervacije-left">
	                                                                <p class="rezervacije-date">14.02.2020. u 15.00h</p>
	                                                                <p>Trajni lak + čupanje obrva</p>
	                                                                <p class="rezervacije-username">Username123</p>
	                                                            </div>
	                                                            <div class="rezervacije-right">
	                                                                <div class="rezervacije-btns">
	                                                                    <a class="btn fs-btn small"><span class="fas fa-check"></span></a>
	                                                                    <a class="btn fs-btn small"><span class="fas fa-times"></span></a>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div id="nadzorna-termini" class="col-12 col-sm-6 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4><span>22.02.2020. </span>Termini</h4>
	                                                    <a class="btn fs-btn dark">Vidi sve</a>
	                                                </div>
	                                                <div class="container">
	                                                    <div class="row">
	                                                        <div class="col-12 rezervacije-div">
	                                                            <div class="rezervacije-left">
	                                                                <p class="rezervacije-date">14.02.2020. u 12.00h</p>
	                                                                <p>Trajni lak + čupanje obrva + feniranje + pramenovi</p>
	                                                                <p class="rezervacije-username">Username123</p>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 rezervacije-div">
	                                                            <div class="rezervacije-left">
	                                                                <p class="rezervacije-date">14.02.2020. u 12.00h</p>
	                                                                <p>Trajni lak + čupanje obrva</p>
	                                                                <p class="rezervacije-username">Username123</p>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 rezervacije-div">
	                                                            <div class="rezervacije-left">
	                                                                <p class="rezervacije-date">14.02.2020. u 12.00h</p>
	                                                                <p>Trajni lak + čupanje obrva</p>
	                                                                <p class="rezervacije-username">Username1234</p>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 col-sm-6 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Komentari</h4>
	                                                    <a class="btn fs-btn dark">Vidi sve</a>
	                                                </div>
	                                                <div class="container">
	                                                    <div class="row">
	                                                        <div class="col-12 komentar-div">
	                                                            <div class="row no-gutters">
	                                                                <div class="col-12 komentar-header">
	                                                                    <div class="komentar-header-left">
	                                                                        <small class="komentar-date">14.02.2020.</small>
	                                                                        <p class="komentar-username">Username1234</p>
	                                                                        <p><span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star"></span>
	                                                                        </p>

	                                                                    </div>
	                                                                    <div class="komentar-header-right">
	                                                                        <div class="komentar-btns">
	                                                                            <a class="btn fs-btn small"><span class="fa fa-reply"></span></a>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                                <div class="col-12 komentar-body">
	                                                                    <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipi elit. Tempore earum delectus molestias aut dolor fuga, saepe atque exercitationem hic...</p>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 komentar-div">
	                                                            <div class="row no-gutters">
	                                                                <div class="col-12 komentar-header">
	                                                                    <div class="komentar-header-left">
	                                                                        <small class="komentar-date">14.02.2020.</small>
	                                                                        <p class="komentar-username">Username1234</p>
	                                                                        <p><span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star"></span>
	                                                                        </p>

	                                                                    </div>
	                                                                    <div class="komentar-header-right">
	                                                                        <div class="komentar-btns">
	                                                                            <a class="btn fs-btn small"><span class="fa fa-reply"></span></a>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                                <div class="col-12 komentar-body">
	                                                                    <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipi elit. Tempore earum delectus molestias aut dolor fuga, saepe atque exercitationem hic...</p>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- termini -->
	                                <div class="tab-pane fade" id="biz-termini" role="tabpanel">
	                                    <div class="container">
	                                        <div class="row">
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Termini</h4>
	                                                    <div class="btn-group my-3 my-sm-0">
	                                                        <a href="#" data-toggle="modal" data-target="#newResModal" class="btn fs-btn dark"><span class="fas fa-plus fa-fw"></span> Dodaj termin</a>
	                                                    </div>
	                                                </div>
	                                                <div class="container mb-3">
	                                                    <div class="row">
	                                                        <div class="col-12 col-sm-6">
	                                                            <div class="biz-panel-title">
	                                                                <a href="#">
	                                                                    <i class="fa fa-arrow-left"></i>
	                                                                </a>
	                                                                <h4><span>22.02.2020. </span>Termini</h4>
	                                                                <a href="#">
	                                                                    <i class="fa fa-arrow-right"></i>
	                                                                </a>
	                                                            </div>
	                                                            <div class="container">
	                                                                <div class="row">
	                                                                    <div class="col-12 rezervacije-div">
	                                                                        <div class="rezervacije-left">
	                                                                            <p class="rezervacije-date">14.02.2020. u 12.00h</p>
	                                                                            <p>Trajni lak + čupanje obrva + feniranje + pramenovi</p>
	                                                                            <p class="rezervacije-username">Username123</p>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-12 rezervacije-div">
	                                                                        <div class="rezervacije-left">
	                                                                            <p class="rezervacije-date">14.02.2020. u 12.00h</p>
	                                                                            <p>Trajni lak + čupanje obrva</p>
	                                                                            <p class="rezervacije-username">Username123</p>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-12 rezervacije-div">
	                                                                        <div class="rezervacije-left">
	                                                                            <p class="rezervacije-date">14.02.2020. u 12.00h</p>
	                                                                            <p>Trajni lak + čupanje obrva</p>
	                                                                            <p class="rezervacije-username">Username1234</p>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 col-sm-6">
	                                                            <div class="biz-panel-title">
	                                                                <h4>Zahtjevi</h4>
	                                                            </div>
	                                                            <div class="container">
	                                                                <div class="row">
	                                                                    <div class="col-12 rezervacije-div">
	                                                                        <div class="rezervacije-left">
	                                                                            <p class="rezervacije-date">14.02.2020. u 12.00h</p>
	                                                                            <p>Trajni lak + čupanje obrva + feniranje + pramenovi</p>
	                                                                            <p class="rezervacije-username">Username123</p>
	                                                                        </div>
	                                                                        <div class="rezervacije-right">
	                                                                            <div class="rezervacije-btns">
	                                                                                <a class="btn fs-btn small"><span class="fas fa-check"></span></a>
	                                                                                <a class="btn fs-btn small"><span class="fas fa-times"></span></a>
	                                                                            </div>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-12 rezervacije-div">
	                                                                        <div class="rezervacije-left">
	                                                                            <p class="rezervacije-date">14.02.2020. u 13.00h</p>
	                                                                            <p>feniranje</p>
	                                                                            <p class="rezervacije-username">Username123</p>
	                                                                        </div>
	                                                                        <div class="rezervacije-right">
	                                                                            <div class="rezervacije-btns">
	                                                                                <a class="btn fs-btn small"><span class="fas fa-check"></span></a>
	                                                                                <a class="btn fs-btn small"><span class="fas fa-times"></span></a>
	                                                                            </div>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-12 rezervacije-div">
	                                                                        <div class="rezervacije-left">
	                                                                            <p class="rezervacije-date">14.02.2020. u 15.00h</p>
	                                                                            <p>Trajni lak + čupanje obrva</p>
	                                                                            <p class="rezervacije-username">Username123</p>
	                                                                        </div>
	                                                                        <div class="rezervacije-right">
	                                                                            <div class="rezervacije-btns">
	                                                                                <a class="btn fs-btn small"><span class="fas fa-check"></span></a>
	                                                                                <a class="btn fs-btn small"><span class="fas fa-times"></span></a>
	                                                                            </div>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Pretplate</h4>
	                                                    <div class="btn-group my-3 my-sm-0">
	                                                        <a href="#" data-toggle="modal" data-target="#newPretModal" class="btn fs-btn dark"><span class="fas fa-plus fa-fw"></span> Dodaj pretplatu</a>
	                                                    </div>
	                                                </div>
	                                                <div class="container mb-3">
	                                                    <div class="row">
	                                                        <div class="col-12 col-sm-6">
	                                                            <p>Nemate novih pretplata</p>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- komentari -->
	                                <div class="tab-pane fade" id="biz-komentari" role="tabpanel">
	                                    <div class="container">
	                                        <div class="row">
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Komentari</h4>
	                                                    <div class="btn-group my-3 my-sm-0">
	                                                        <a href="#" data-toggle="modal" data-target="#newResModal" class="btn fs-btn dark"><span class="fas fa-plus fa-fw"></span> Dodaj termin</a>
	                                                    </div>
	                                                </div>
	                                                <div class="container mb-3">
	                                                    <div class="row">
	                                                        <!-- komentari -->
	                                                        <div class="col-12 komentar-div">
	                                                            <div class="row no-gutters">
	                                                                <div class="col-12 komentar-header">
	                                                                    <div class="komentar-header-left">
	                                                                        <small class="komentar-date">14.02.2020.</small>
	                                                                        <h4 class="komentar-username">Frizerski salon Diamond</h4>
	                                                                        <p><span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star"></span>
	                                                                        </p>

	                                                                    </div>
	                                                                    <div class="komentar-header-right">
	                                                                        <div class="komentar-btns">
	                                                                            <a href="#" data-toggle="modal" data-target="#replyKomModal" class="btn fs-btn small"><span class="fas fa-reply"></span></a>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                                <div class="col-12 komentar-body">
	                                                                    <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore earum delectus molestias aut dolor fuga, saepe atque exercitationem hic. Modi quisquam sit, numquam repellendus asperiores incidunt quam illum nostrum sunt. ipsum dolor sit amet, consectetur adipisicing elit. Magni fugit impedit aliquid nostrum ratione reiciendis incidunt, distinctio labore neque nihil iste voluptate animi quis eum provident sapiente sint rerum non.</p>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 komentar-div">
	                                                            <div class="row no-gutters">
	                                                                <div class="col-12 komentar-header">
	                                                                    <div class="komentar-header-left">
	                                                                        <small class="komentar-date">14.02.2020.</small>
	                                                                        <h4 class="komentar-username">Frizerski salon Diamond</h4>
	                                                                        <p><span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star"></span>
	                                                                        </p>

	                                                                    </div>
	                                                                    <div class="komentar-header-right">
	                                                                        <div class="komentar-btns">
	                                                                            <a class="btn fs-btn small"><span class="fas fa-reply"></span></a>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                                <div class="col-12 komentar-body">
	                                                                    <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore earum delectus molestias aut dolor fuga, saepe atque exercitationem hic. Modi quisquam sit, numquam repellendus asperiores incidunt quam illum nostrum sunt. ipsum dolor sit amet, consectetur adipisicing elit. Magni fugit impedit aliquid nostrum ratione reiciendis incidunt, distinctio labore neque nihil iste voluptate animi quis eum provident sapiente sint rerum non.</p>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 komentar-div">
	                                                            <div class="row no-gutters">
	                                                                <div class="col-12 komentar-header">
	                                                                    <div class="komentar-header-left">
	                                                                        <small class="komentar-date">14.02.2020.</small>
	                                                                        <h4 class="komentar-username">Frizerski salon Diamond</h4>
	                                                                        <p><span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star"></span>
	                                                                        </p>

	                                                                    </div>
	                                                                    <div class="komentar-header-right">
	                                                                        <div class="komentar-btns">
	                                                                            <a class="btn fs-btn small"><span class="fas fa-reply"></span></a>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                                <div class="col-12 komentar-body">
	                                                                    <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore earum delectus molestias aut dolor fuga, saepe atque exercitationem hic. Modi quisquam sit, numquam repellendus asperiores incidunt quam illum nostrum sunt. ipsum dolor sit amet, consectetur adipisicing elit. Magni fugit impedit aliquid nostrum ratione reiciendis incidunt, distinctio labore neque nihil iste voluptate animi quis eum provident sapiente sint rerum non.</p>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <div class="col-12 komentar-div">
	                                                            <div class="row no-gutters">
	                                                                <div class="col-12 komentar-header">
	                                                                    <div class="komentar-header-left">
	                                                                        <small class="komentar-date">14.02.2020.</small>
	                                                                        <h4 class="komentar-username">Frizerski salon Diamond</h4>
	                                                                        <p><span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star checked"></span>
	                                                                            <span class="fa fa-star"></span>
	                                                                        </p>

	                                                                    </div>
	                                                                    <div class="komentar-header-right">
	                                                                        <div class="komentar-btns">
	                                                                            <a class="btn fs-btn small"><span class="fas fa-reply"></span></a>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                                <div class="col-12 komentar-body">
	                                                                    <p class="komentar-tekst">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore earum delectus molestias aut dolor fuga, saepe atque exercitationem hic. Modi quisquam sit, numquam repellendus asperiores incidunt quam illum nostrum sunt. ipsum dolor sit amet, consectetur adipisicing elit. Magni fugit impedit aliquid nostrum ratione reiciendis incidunt, distinctio labore neque nihil iste voluptate animi quis eum provident sapiente sint rerum non.</p>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                        <!-- komentari pagination -->
	                                                        <div class="col-12">
	                                                            <nav aria-label="Page navigation">
	                                                                <ul class="pagination justify-content-center">
	                                                                    <li class="page-item align-self-center">
	                                                                        <a class="page-link" href="#" aria-label="Previous">
	                                                                            <span aria-hidden="true">&laquo;</span>
	                                                                            <span class="sr-only">Previous</span>
	                                                                        </a>
	                                                                    </li>
	                                                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
	                                                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
	                                                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
	                                                                    <li class="page-item">
	                                                                        <a class="page-link" href="#" aria-label="Next">
	                                                                            <span aria-hidden="true">&raquo;</span>
	                                                                            <span class="sr-only">Next</span>
	                                                                        </a>
	                                                                    </li>
	                                                                </ul>
	                                                            </nav>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- usluge -->
	                                <div class="tab-pane fade" id="biz-usluge" role="tabpanel">
	                                    <div class="container">
	                                        <div class="row">
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Usluge</h4>
	                                                    <div class="btn-group my-3 my-sm-0">
	                                                        <a href="#" data-toggle="modal" data-target="#addServModal" class="btn fs-btn dark"><span class="fas fa-plus fa-fw"></span> Dodaj uslugu</a>
	                                                    </div>
	                                                </div>
	                                                <div class="container mb-3">
	                                                    <div class="row">
	                                                        <div class="col-12">
	                                                            <table id="table-frizure" class="table table-sm">
	                                                                <tbody>
	                                                                    <tr>
	                                                                        <td class="text-left">
	                                                                            <p>žensko šišanje<span class="badge badge-danger akcija">AKCIJA!</span></p>
	                                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                                        </td>
	                                                                        <td class="text-right td-flex">
	                                                                            <div class="td-2 mr-3">
	                                                                                <p>50,00 kn</p>
	                                                                                <small>30 min</small>
	                                                                            </div>
	                                                                            <div class="td-1">
	                                                                                <a href="#" data-toggle="modal" data-target="#editServModal" class="btn fs-btn small"><span class="far fa-edit"></span></a>
	                                                                                <a href="#" data-toggle="modal" data-target="#delServModal" class="btn fs-btn small"><span class="far fa-trash-alt"></span></a>
	                                                                            </div>
	                                                                        </td>
	                                                                    </tr>
	                                                                    <tr>
	                                                                        <td class="text-left">
	                                                                            <p>Maska za kosu</p>
	                                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                                        </td>
	                                                                        <td class="text-right td-flex">
	                                                                            <div class="td-2 mr-3">
	                                                                                <p>20,00 kn</p>
	                                                                                <small>1.20 h</small>
	                                                                            </div>
	                                                                            <div class="td-1">
	                                                                                <a href="#" data-toggle="modal" data-target="#edit-service-modal" class="btn fs-btn small"><span class="far fa-edit"></span></a>
	                                                                                <a href="#" data-toggle="modal" data-target="#del-service-modal" class="btn fs-btn small"><span class="far fa-trash-alt"></span></a>
	                                                                            </div>
	                                                                        </td>
	                                                                    </tr>
	                                                                    <tr>
	                                                                        <td class="text-left">
	                                                                            <p>Pranje kose</p>
	                                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                                        </td>
	                                                                        <td class="text-right td-flex">
	                                                                            <div class="td-2 mr-3">
	                                                                                <p>30,00 kn</p>
	                                                                                <small>10 min</small>
	                                                                            </div>
	                                                                            <div class="td-1">
	                                                                                <a href="#" data-toggle="modal" data-target="#edit-service-modal" class="btn fs-btn small"><span class="far fa-edit"></span></a>
	                                                                                <a href="#" data-toggle="modal" data-target="#del-service-modal" class="btn fs-btn small"><span class="far fa-trash-alt"></span></a>
	                                                                            </div>
	                                                                        </td>
	                                                                    </tr>
	                                                                    <tr>
	                                                                        <td class="text-left">
	                                                                            <p>Pranje + fen ravno</p>
	                                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                                        </td>
	                                                                        <td class="text-right td-flex">
	                                                                            <div class="td-2 mr-3">
	                                                                                <p>45,00 kn</p>
	                                                                                <small>30 min</small>
	                                                                            </div>
	                                                                            <div class="td-1">
	                                                                                <a href="#" data-toggle="modal" data-target="#edit-service-modal" class="btn fs-btn small"><span class="far fa-edit"></span></a>
	                                                                                <a href="#" data-toggle="modal" data-target="#del-service-modal" class="btn fs-btn small"><span class="far fa-trash-alt"></span></a>
	                                                                            </div>
	                                                                        </td>
	                                                                    </tr>
	                                                                    <tr>
	                                                                        <td class="text-left">
	                                                                            <p>Pranje + fen lokne</p>
	                                                                            <small>Kratki opis usluge da nije duži od ovog.</small>
	                                                                        </td>
	                                                                        <td class="text-right td-flex">
	                                                                            <div class="td-2 mr-3">
	                                                                                <p>55,00 kn</p>
	                                                                                <small>35 min</small>
	                                                                            </div>
	                                                                            <div class="td-1">
	                                                                                <a href="#" data-toggle="modal" data-target="#edit-service-modal" class="btn fs-btn small"><span class="far fa-edit"></span></a>
	                                                                                <a href="#" data-toggle="modal" data-target="#del-service-modal" class="btn fs-btn small"><span class="far fa-trash-alt"></span></a>
	                                                                            </div>
	                                                                        </td>
	                                                                    </tr>
	                                                                </tbody>
	                                                            </table>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- galerija -->
	                                <div class="tab-pane fade" id="biz-galerija" role="tabpanel">
	                                    <div class="container">
	                                        <div class="row">
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Galerija</h4>
	                                                </div>
	                                                <div class="container mb-3">
	                                                    <div class="row">
	                                                        <div class="col-md-6 mb-3">
	                                                            <form method="post" action="#" id="#">
	                                                                <div class="form-group files">
	                                                                    <input type="file" class="form-control" multiple="">
	                                                                </div>
	                                                                <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Učitaj</button>
	                                                            </form>
	                                                        </div>
	                                                        <div class="col-md-6 mb-3">
	                                                            <table class="table table-hover table-borderless bizInfoTable">
	                                                                <tr>
	                                                                    <th>Naslovna:</th>
	                                                                    <td class="bizInfoInput"><span>naslovna-slika.jpeg</span></td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Profilna:</th>
	                                                                    <td class="bizInfoInput"><span>profilna-slika.jpeg</span></td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Istaknute:</th>
	                                                                    <td class="bizInfoInput-slike">
	                                                                        <span>naslovna-slika.jpeg</span>
	                                                                        <span>naslovna-slika.jpeg</span>
	                                                                        <span>maloduzitekstzanaslovnu-slikukanafejsu.jpeg</span>
	                                                                        <span>naslovna-slika.jpeg</span>
	                                                                        <span>naslovna-slika.jpeg</span>
	                                                                        <span>naslovna-slika.jpeg</span>
	                                                                        <span>naslovna-slika.jpeg</span>
	                                                                    </td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                            </table>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="container mb-3">
	                                                    <div class="row">
	                                                        <div class="col-12 mb-3">
	                                                            <div class="row mt-3 no-gutters">
	                                                                <?php
	                                                                        $images1 = array();
	                                                                        $files = glob("img/images/*.*");
	                                                                        foreach ($files as $image) {
	                                                                        $images1[] = $image;
	                                                                        }

	                                                                        for( $i = 0, $count = count( $images1 ) - 1; $i < 24; $i++ ){
	                                                                            $showing = $images1[$i];

	                                                                            echo '<div class="col-4 col-md-2">
	                                                                                  <div class="imgContainer">
	                                                                                        <a href="' . $showing . '" data-lightbox="'.basename(dirname($showing)).'" data-title="'.basename($showing).'" title="Preview">
	                                                                                            <div class="content-box-pic">
	                                                                                            <div class="img-wrapper">
	                                                                                                <img src="' . $showing . '" class="img-responsive" alt="'.basename(dirname($showing)).'" />
	                                                                                            </div>
	                                                                                            </div>
	                                                                                        </a>
	                                                                                    </div>
	                                                                                  </div>'; 
	                                                                        }
	                                                                    ?>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- salon -->
	                                <div class="tab-pane fade" id="biz-salon" role="tabpanel">
	                                    <div class="container">
	                                        <div class="row">
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Salon</h4>
	                                                    <div class="btn-group my-3 my-sm-0">
	                                                        <a href="#" data-toggle="modal" data-target="#addServModal" class="btn fs-btn dark"><span class="fas fa-plus fa-fw"></span> Dodaj salon</a>
	                                                    </div>
	                                                </div>
	                                                <div class="container mb-3">
	                                                    <div class="row no-gutters">
	                                                        <div class="col-12">
	                                                            <table class="table table-hover table-borderless bizInfoTable">
	                                                                <tr>
	                                                                    <th>Naziv Salona:</th>
	                                                                    <td class="bizInfoInput">Frizerski salon Diamond tratadjkfasdgf</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Kratki opis:</th>
	                                                                    <td class="bizInfoInput">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, impedit sequi rem quasi odio aspernatur odit exercitationem nostrum consectetur voluptatem dolore doloremque, ad mollitia vero veritatis laborum nesciunt sed maiores.</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Kontakt:</th>
	                                                                    <td class="bizInfoInput">0987654321</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Adresa:</th>
	                                                                    <td class="bizInfoInput">Duži naziv ulice 145, Mjesto</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Radno vrijeme:</th>
	                                                                    <td class="bizInfoInput"></td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Zaposlenici:</th>
	                                                                    <td class="bizInfoInput"></td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Svojstva:</th>
	                                                                    <td class="bizInfoInput"></td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Društvene mreže:</th>
	                                                                    <td class="bizInfoInput"></td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                            </table>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- zaposlenici -->
	                                <div class="tab-pane fade" id="biz-zaposlenici" role="tabpanel">
	                                    <div class="container">
	                                        <div class="row">
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Zaposlenici</h4>
	                                                    <div class="btn-group my-3 my-sm-0">
	                                                        <a href="#" data-toggle="modal" data-target="#addServModal" class="btn fs-btn"><span class="fas fa-plus fa-fw"></span></a>
	                                                        <a href="#" data-toggle="modal" data-target="#addServModal" class="btn fs-btn"><span class="far fa-trash-alt fa-fw"></span></a>
	                                                    </div>
	                                                </div>
	                                                <div class="container mb-3">
	                                                    <div class="row no-gutters">
	                                                        <div class="col-12">
	                                                            <table class="table table-hover table-borderless bizInfoTable">
	                                                                <tr>
	                                                                    <th>Ime i prezime:</th>
	                                                                    <td class="bizInfoInput">Name Surname</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Razina dopuštenja:</th>
	                                                                    <td class="bizInfoInput">administrator</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Radno mjesto:</th>
	                                                                    <td class="bizInfoInput">Frizer, pediker</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Poslovnica:</th>
	                                                                    <td class="bizInfoInput">Slavićeva</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Kontakt:</th>
	                                                                    <td class="bizInfoInput">0987654321</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Usluge:</th>
	                                                                    <td class="bizInfoInput">feniranje, muško šišanje, žensko šišanje, feniranje, pramenovi, maska za kosu, ekstenzije, bojanje, trajni lak, umjetni nokti, usluga 1, usluga 2, usluga x, usluga y</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                            </table>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="container mb-3">
	                                                    <div class="row no-gutters">
	                                                        <div class="col-12">
	                                                            <table class="table table-hover table-borderless bizInfoTable">
	                                                                <tr>
	                                                                    <th>Ime i prezime:</th>
	                                                                    <td class="bizInfoInput">Name Surname</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Razina dopuštenja:</th>
	                                                                    <td class="bizInfoInput">urednik</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Radno mjesto:</th>
	                                                                    <td class="bizInfoInput">Frizer</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Poslovnica:</th>
	                                                                    <td class="bizInfoInput">Slavićeva</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Kontakt:</th>
	                                                                    <td class="bizInfoInput">0987654321</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Usluge:</th>
	                                                                    <td class="bizInfoInput">feniranje, muško šišanje, žensko šišanje, feniranje, pramenovi, maska za kosu, ekstenzije, bojanje, trajni lak, umjetni nokti, usluga 1, usluga 2, usluga x, usluga y</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                            </table>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="container mb-3">
	                                                    <div class="row no-gutters">
	                                                        <div class="col-12">
	                                                            <table class="table table-hover table-borderless bizInfoTable">
	                                                                <tr>
	                                                                    <th>Ime i prezime:</th>
	                                                                    <td class="bizInfoInput">Name Surname</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Razina dopuštenja:</th>
	                                                                    <td class="bizInfoInput">korisnik</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Radno mjesto:</th>
	                                                                    <td class="bizInfoInput">Frizer</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Poslovnica:</th>
	                                                                    <td class="bizInfoInput">Slavićeva</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Kontakt:</th>
	                                                                    <td class="bizInfoInput">0987654321</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Usluge:</th>
	                                                                    <td class="bizInfoInput">feniranje, muško šišanje, žensko šišanje, feniranje, pramenovi, maska za kosu, ekstenzije, bojanje, trajni lak, umjetni nokti, usluga 1, usluga 2, usluga x, usluga y</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                            </table>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- postavke -->
	                                <div class="tab-pane fade" id="biz-postavke" role="tabpanel">
	                                    <div class="container">
	                                        <div class="row">
	                                            <div class="col-12 salon-container mb-3">
	                                                <div class="biz-panel-title">
	                                                    <h4>Postavke profila</h4>
	                                                </div>
	                                                <div class="container mb-3">
	                                                    <div class="row no-gutters">
	                                                        <div class="col-12">
	                                                            <table class="table table-hover table-borderless bizInfoTable">
	                                                                <tr>
	                                                                    <th>Korisničko ime:</th>
	                                                                    <td class="bizInfoInput">Username123</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>E-adresa:</th>
	                                                                    <td class="bizInfoInput">mail@service.com</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Lozinka:</th>
	                                                                    <td class="bizInfoInput">*************</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Kontakt:</th>
	                                                                    <td class="bizInfoInput">0987654321</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <th>Obavijesti:</th>
	                                                                    <td class="bizInfoInput">Whatsapp</td>
	                                                                    <td class="bizInfoLink">uredi</td>
	                                                                </tr>
	                                                            </table>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div id="side-content" class="col-lg-3 col-xl-3 pb-5 order-lg-1">
	                <div class="salon-container container stickey-top">
	                    <div class="nav navbar-nav user-menu-vert" role="tablist">
	                        <a class="nav-item nav-link active" data-toggle="pill" href="#biz-nadzorna" role="tab"><span class="fas fa-tachometer-alt fa-fw"></span>Nadzorna ploča</a>
	                        <a class="nav-item nav-link" data-toggle="pill" href="#biz-termini" role="tab"><span class="far fa-calendar-check fa-fw"></span>Termini</a>
	                        <a class="nav-item nav-link" data-toggle="pill" href="#biz-komentari" role="tab"><span class="far fa-comments fa-fw"></span>Komentari</a>
	                        <a class="nav-item nav-link" data-toggle="pill" href="#biz-usluge" role="tab"><span class="fas fa-hand-holding-usd fa-fw"></span>Usluge</a>
	                        <a class="nav-item nav-link" data-toggle="pill" href="#biz-galerija" role="tab"><span class="far fa-images fa-fw"></span>Galerija</a>
	                        <a class="nav-item nav-link" data-toggle="pill" href="#biz-salon" role="tab"><span class="fas fa-store-alt fa-fw"></span>Salon</a>
	                        <a class="nav-item nav-link" data-toggle="pill" href="#biz-zaposlenici" role="tab"><span class="fas fa-user-friends fa-fw"></span>Zaposlenici</a>
	                        <a class="nav-item nav-link" data-toggle="pill" href="#biz-postavke" role="tab"><span class="fas fa-cog fa-fw"></span>Postavke profila</a>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="modal animated fadeIn" id="newResModal" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header text-center">
	                    <h4 class="modal-title w-100 font-weight-bold">Novi termin</h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body mx-3">
	                    <form id="" action="" method="post">
	                        <div class="row">
	                            <div class="col-6 input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt fa-fw"></i></span>
	                                </div>
	                                <input id="datum" name="datum" type="text" class="form-control" placeholder="Datum" aria-label="datum" aria-describedby="datum" required>
	                            </div>
	                            <div class="col-6 input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <span class="input-group-text" id="basic-addon1"><i class="far fa-clock fa-fw"></i></span>
	                                </div>
	                                <input id="vrijeme" name="vrijeme" type="text" class="form-control" placeholder="Vrijeme" aria-label="name" aria-describedby="basic-addon1" required>
	                            </div>
	                        </div>
	                        <div class="input-group mb-3">
	                            <div class="input-group-prepend">
	                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
	                            </div>
	                            <input id="klijent" name="klijent" type="text" class="form-control" placeholder="Klijent" aria-label="klijent" aria-describedby="basic-addon1" required>
	                        </div>
	                        <div class="input-group mb-3">
	                            <div class="input-group-prepend">
	                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-hand-holding-usd fa-fw"></i></span>
	                            </div>
	                            <input id="usluga" name="usluga" type="text" class="form-control" placeholder="Usluga" aria-label="klijent" aria-describedby="basic-addon1" required>
	                        </div>
	                        <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Spremi</button>
	                    </form>
	                </div>
	                <div id="book-modal-footer" class="modal-footer">
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="modal animated fadeIn" id="newPretModal" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header text-center">
	                    <h4 class="modal-title w-100 font-weight-bold">Nova pretplata</h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body mx-3">
	                    <form id="" action="" method="post">
	                        <div class="row">
	                            <div class="col-6 input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt fa-fw"></i></span>
	                                </div>
	                                <input id="datum" name="datum" type="text" class="form-control" placeholder="Datum" aria-label="datum" aria-describedby="datum" required>
	                            </div>
	                            <div class="col-6 input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <span class="input-group-text" id="basic-addon1"><i class="far fa-clock fa-fw"></i></span>
	                                </div>
	                                <input id="vrijeme" name="vrijeme" type="text" class="form-control" placeholder="Vrijeme" aria-label="name" aria-describedby="basic-addon1" required>
	                            </div>
	                        </div>
	                        <div class="input-group mb-3">
	                            <div class="input-group-prepend">
	                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
	                            </div>
	                            <input id="klijent" name="klijent" type="text" class="form-control" placeholder="Klijent" aria-label="klijent" aria-describedby="basic-addon1" required>
	                        </div>
	                        <div class="input-group mb-3">
	                            <div class="input-group-prepend">
	                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-hand-holding-usd fa-fw"></i></span>
	                            </div>
	                            <input id="usluga" name="usluga" type="text" class="form-control" placeholder="Usluga" aria-label="klijent" aria-describedby="basic-addon1" required>
	                        </div>
	                        <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Spremi</button>
	                    </form>
	                </div>
	                <div id="book-modal-footer" class="modal-footer">
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="modal animated fadeIn" id="replyKomModal" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header text-center">
	                    <h4 class="modal-title w-100 font-weight-bold">Odgovori na komentar</h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body mx-3">
	                    <form id="" action="" method="post">
	                        <div class="row">
	                            <div class="col-12 mb-3">
	                                <textarea class="form-control" id="servArea" rows="3" placeholder="Napišite vaš odgovor na komentar" aria-label="odgovor na komentar" aria-describedby="odgovor na komentar" required></textarea>
	                            </div>
	                        </div>
	                        <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Pošalji</button>
	                    </form>
	                </div>
	                <div id="book-modal-footer" class="modal-footer">
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="modal animated fadeIn" id="addServModal" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header text-center">
	                    <h4 class="modal-title w-100 font-weight-bold">Dodaj uslugu</h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body mx-3">
	                    <form id="" action="" method="post">
	                        <div class="row">
	                            <div class="col-12 mb-3">
	                                <input id="servName" name="servName" type="text" class="form-control" placeholder="Usluga" aria-label="usluga" aria-describedby="usluga" required>
	                            </div>
	                            <div class="col-12 mb-3">
	                                <textarea class="form-control" id="servArea" rows="3" placeholder="kratki opis usluge" aria-label="opis usluge" aria-describedby="opis usluge" required></textarea>
	                            </div>
	                            <div class="col-6 mb-3">
	                                <input id="servPrice" name="servPrice" type="number" class="form-control" placeholder="Cijena" aria-label="cijena usluge" aria-describedby="cijena usluge" required>
	                            </div>
	                            <div class="col-6 mb-3">
	                                <input id="servTime" name="servTime" type="number" class="form-control" placeholder="Vrijeme (min)" aria-label="vrijeme trajanja" aria-describedby="vrijeme trajanja" required>
	                            </div>
	                            <div class="col-12 mb-3 text-center">
	                                <div class="form-check form-check-inline">
	                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
	                                    <label class="form-check-label" for="inlineCheckbox1">Istaknuto</label>
	                                </div>
	                                <div class="form-check form-check-inline">
	                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
	                                    <label class="form-check-label" for="inlineCheckbox1">Akcija</label>
	                                </div>
	                            </div>
	                        </div>
	                        <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Dodaj</button>
	                    </form>
	                </div>
	                <div id="book-modal-footer" class="modal-footer">
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="modal animated fadeIn" id="editServModal" tabindex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header text-center">
	                    <h4 class="modal-title w-100 font-weight-bold">Uredi uslugu</h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body mx-3">
	                    <form id="" action="" method="post">
	                        <div class="row">
	                            <div class="col-12 mb-3">
	                                <input id="servName" name="servName" type="text" class="form-control" placeholder="Usluga" aria-label="usluga" aria-describedby="usluga" required>
	                            </div>
	                            <div class="col-12 mb-3">
	                                <textarea class="form-control" id="servArea" rows="3" placeholder="kratki opis usluge" aria-label="opis usluge" aria-describedby="opis usluge" required></textarea>
	                            </div>
	                            <div class="col-6 mb-3">
	                                <input id="servPrice" name="servPrice" type="number" class="form-control" placeholder="Cijena" aria-label="cijena usluge" aria-describedby="cijena usluge" required>
	                            </div>
	                            <div class="col-6 mb-3">
	                                <input id="servTime" name="servTime" type="number" class="form-control" placeholder="Vrijeme (min)" aria-label="vrijeme trajanja" aria-describedby="vrijeme trajanja" required>
	                            </div>
	                            <div class="col-12 mb-3 text-center">
	                                <div class="form-check form-check-inline">
	                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
	                                    <label class="form-check-label" for="inlineCheckbox1">Istaknuto</label>
	                                </div>
	                                <div class="form-check form-check-inline">
	                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
	                                    <label class="form-check-label" for="inlineCheckbox1">Akcija</label>
	                                </div>
	                            </div>
	                        </div>
	                        <button id="submit" name="submit" type="submit" type="submit" class="fs-btn dark text-uppercase btn-lg btn-block">Spremi</button>
	                    </form>
	                </div>
	                <div id="book-modal-footer" class="modal-footer">
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="modal animated fadeIn" id="delServModal" tabindex="-1" role="dialog" aria-labelledby="dekKomModal" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered" role="document">
	            <div class="modal-content">
	                <div class="modal-header text-center">
	                    <h4 class="modal-title w-100 font-weight-bold">Brisanje usluge</h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body mx-3">
	                    <div class="row">
	                        <div class="col-12 text-center mb-3">
	                            <h4>Jeste li sigurni da želite izbrisati uslugu?</h4>
	                        </div>
	                        <div class="col-12 text-center">
	                            <div class="btn-group">
	                                <a class="btn fs-btn dark mr-2">Izbriši</a>
	                                <a class="btn fs-btn primary">Odustani</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div id="book-modal-footer" class="modal-footer">
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	@include('inc.footer')

@endsection