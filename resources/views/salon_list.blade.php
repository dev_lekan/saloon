@extends('inc.asset')
@section('content')
	@include('inc.nav')
	{{-- @include('inc.search') --}}
	<div class="main-top">

	    <div id="search-bar">
	        <div class="container">
	            @include('inc.search')
	        </div>
	    </div>

	    <div id="main-content-area" class="container">
	        <div class="row no-gutters">

	            <div id="main-content" class="col-lg-9 col-xl-9 order-lg-12">
	                <div id="salon-container" class="container">
	                    <div class="row">
	                        <div class="col-12">
	                            <div class="saloni-list-heading">
	                                <span>Pronađeno 48 salona</span>
	                                <form class="saloni-list-sort">
	                                    <select class="custom-select">
	                                        <option value="">nasumično</option>
	                                        <option value="1">cijena</option>
	                                        <option value="2">udaljenost</option>
	                                        <option value="3">ocjena</option>
	                                    </select>
	                                </form>
	                            </div>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                        <div class="col-12">
	                            <a href="/salon">
	                                <div class="content-box-list">
	                                    <div class="row">
	                                        <div class="col-md-5">
	                                            <div class="row no-gutters">
	                                                <div class="col-12">
	                                                    <div class="img-wrapper">
	                                                        <img src="img/salon.jpg" alt="" class="img-responsive">
	                                                    </div>   
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/nokti/nokti11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/images/makeup/makeup11.jpg" />
	                                                    </div>
	                                                </div>
	                                                <div class="col-4">
	                                                    <div class="imgContainer">
	                                                        <img src="img/salon%206.jpeg" />
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-7">
	                                            <h4>Frizerski salon Diamond</h4>
	                                            <p class="saloni-list-adresa">Domovinskog rata 123, Split</p>
	                                            <div class="saloni-list-stats">
	                                                <div class="saloni-list-ratings">
	                                                    <span class="fa fa-star checked"> </span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star checked"></span>
	                                                    <span class="fa fa-star mr-1"></span><small> (36)</small>
	                                                </div>
	                                                <div class="saloni-list-comm">
	                                                    <span class="fa fa-comments"></span><small> (43)</small>
	                                                </div>
	                                                <div class="saloni-list-heart">
	                                                    <span class="fa fa-heart"></span><small> (12)</small>
	                                                </div>

	                                            </div>
	                                            <table id="table-frizure" class="table table-sm">
	                                                <tbody>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje kose</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>30,00 kn</p>
	                                                                <small>10 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen ravno</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>45,00 kn</p>
	                                                                <small>30 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="text-left">
	                                                            <p>Pranje + fen lokne</p>
	                                                            <small>Kratki opis usluge da nije duži.</small>
	                                                        </td>
	                                                        <td class="text-right td-flex">
	                                                            <div class="td-2 mr-3">
	                                                                <p>55,00 kn</p>
	                                                                <small>35 min</small>
	                                                            </div>
	                                                            <div class="td-1"><a class="btn fs-btn small dark">Book</a></div>
	                                                        </td>
	                                                    </tr>
	                                                </tbody>
	                                            </table>
	                                        </div>
	                                    </div>
	                                </div>
	                            </a>
	                        </div>
	                    </div>
	                    <nav aria-label="Page navigation">
	                        <ul class="pagination justify-content-center">
	                            <li class="page-item">
	                                <a class="page-link" href="#" aria-label="Previous">
	                                    <span aria-hidden="true">&laquo;</span>
	                                    <span class="sr-only">Previous</span>
	                                </a>
	                            </li>
	                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
	                            <li class="page-item"><a class="page-link" href="#">2</a></li>
	                            <li class="page-item"><a class="page-link" href="#">3</a></li>
	                            <li class="page-item">
	                                <a class="page-link" href="#" aria-label="Next">
	                                    <span aria-hidden="true">&raquo;</span>
	                                    <span class="sr-only">Next</span>
	                                </a>
	                            </li>
	                        </ul>
	                    </nav>
	                </div>

	            </div>

	            <div id="side-content" class="col-lg-3 col-xl-3 pb-5 order-lg-1">
	                <div class="salon-container container stickey-top">
	                    <div class="side-content-heading">
	                        Filtriraj potragu
	                    </div>
	                    <form name="search-sidebar-form" id="search-sidebar-form" method="" action="" onSubmit="">
	                        <!--
	                            <div class="form-group">
	                                <label for="Datum">Datum</label>
	                                <input type="text" id="datepicker" name="datepicker" class="form-control date-picker" name="start" placeholder="Bilo kada " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Bilo kada '" required readonly="true" value="">
	                            </div>
	                            <div class="form-group">
	                                <label for="exampleInputEmail1">Vrijeme</label>
	                                <select type="text" class="form-control">
	                                    <option value="" disabled selected>Bilo kada</option>
	                                    <option value="08.00">08.00 h</option>
	                                    <option value="09.00">09.00 h</option>
	                                    <option value="10.00">10.00 h</option>
	                                    <option value="11.00">11.00 h</option>
	                                    <option value="12.00">12.00 h</option>
	                                    <option value="13.00">13.00 h</option>
	                                    <option value="14.00">14.00 h</option>
	                                    <option value="15.00">15.00 h</option>
	                                    <option value="16.00">16.00 h</option>
	                                    <option value="17.00">17.00 h</option>
	                                    <option value="18.00">18.00 h</option>
	                                    <option value="19.00">19.00 h</option>
	                                </select>
	                            </div>
	                            -->
	                        <div class="form-group">
	                            <label for="exampleInputEmail1">Cijena</label>
	                            <div class="input-group">
	                                <input type="text" class="form-control">
	                                <div id="label-do" class="input-group-prepend">
	                                    <small>DO</small>
	                                </div>
	                                <input type="text" class="form-control">
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label for="exampleInputEmail1">Ocjena</label>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-ocjena-5">
	                                <label class="custom-control-label" for="check-ocjena-5"><span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span> (5)</label>
	                            </div>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-ocjena-4">
	                                <label class="custom-control-label" for="check-ocjena-4"><span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star"></span> (4)</label>
	                            </div>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-ocjena-3">
	                                <label class="custom-control-label" for="check-ocjena-3"><span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star"></span>
	                                    <span class="fa fa-star"></span> (3)</label>
	                            </div>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-ocjena-2">
	                                <label class="custom-control-label" for="check-ocjena-2"><span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star"></span>
	                                    <span class="fa fa-star"></span>
	                                    <span class="fa fa-star"></span> (2)</label>
	                            </div>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-ocjena-1">
	                                <label class="custom-control-label" for="check-ocjena-1"><span class="fa fa-star checked"></span>
	                                    <span class="fa fa-star"></span>
	                                    <span class="fa fa-star"></span>
	                                    <span class="fa fa-star"></span>
	                                    <span class="fa fa-star"></span> (1)</label>
	                            </div>

	                        </div>
	                        <div class="form-group">
	                            <label for="exampleInputEmail1">Ostalo</label>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-akcija">
	                                <label class="custom-control-label" for="check-akcija">Akcija</label>
	                            </div>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-vikend">
	                                <label class="custom-control-label" for="check-vikend">Otvoreni vikendom</label>
	                            </div>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-adresa">
	                                <label class="custom-control-label" for="check-adresa">Dolazak na adresu</label>
	                            </div>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-adresa">
	                                <label class="custom-control-label" for="check-adresa">Pristup osobama s invaliditetom</label>
	                            </div>
	                            <div class="custom-control custom-checkbox">
	                                <input type="checkbox" class="custom-control-input" id="check-adresa">
	                                <label class="custom-control-label" for="check-adresa">Dostupan parking</label>
	                            </div>
	                        </div>
	                        <div class="text-center form-group">
	                            <button class="btn fs-btn small dark" id="">Pretraži</button>
	                        </div>
	                </div>
	                </form>
	            </div>

	        </div>
	    </div>

	</div>
	@include('inc.footer')
@endsection